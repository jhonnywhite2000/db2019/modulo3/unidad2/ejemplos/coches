<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Alquileres';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="alquileres-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Alquileres', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],

            'codigoAlquiler',
            //'usuario',
            //'usuario0.nombre',
            [
            'label'=>'Nombre del usuario',
            'format'=>'raw',
            'content'=>function($model){
                return Html::a('ver alquileres de ' . $model->usuario0->nombre, ['alquileres/alquileresusuarios', 'id'=>$model->usuario],
                        ['class'=>'btn btn-primary']
                        
                        );
            }     
            ],
            'coche',
            [
            'label'=>'Coche alquilado',
            'format'=>'raw',
            'content'=>function($model){
                return Html::a('ver alquileres del coche ' . $model->coche, ['alquileres/alquilerescoche', 'id'=>$model->coche],
                        ['class'=>'btn btn-primary']
                        
                        );
            }     
            ],       
            'coche0.marca',
            [
            'label'=>'Marca Coche',
            'format'=>'raw',
            'content'=>function($model){
                return Html::a('ver alquileres de coches ' . $model->coche0->marca, ['alquileres/alquileresmarca', 'id'=>$model->coche0->marca],
                        ['class'=>'btn btn-primary']
                        
                        );
            }     
            ],        
            'fecha',
             [
            'label'=>'Fecha de Alquiler',
            'format'=>'raw',
            'content'=>function($model){
                return Html::a('ver alquileres del año de la fecha ' . $model->fecha, ['alquileres/alquileresyear', 'id'=>$model->codigoAlquiler],
                        ['class'=>'btn btn-primary']
                        
                        );
            }     
            ],       
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
